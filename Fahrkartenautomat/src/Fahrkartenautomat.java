﻿import java.text.DecimalFormat;
import java.util.Scanner;

class Fahrkartenautomat
{
	static double[] preis = new double[]{2.9, 3.3, 3.6, 1.9, 8.6, 23.5, 24.3, 24.9};
    static String[] bezeichnung = new String[]
		 {"Einzelfahrschein Berlin AB", 
			 "Einzelfahrschein Berlin BC", 
			 "Einzelfahrschein Berlin ABC",
			 "Kurzstrecke",
			 "Tageskarte Berlin AB",
			 "Tageskarte Berlin BC",
			 "Tageskarte Berlin ABC",
			 "Kleingruppen-Tageskarte Berlin AB",
			 "Kleingruppen-Tageskarte Berlin BC",
			 "Kleingruppen-Tageskarte Berlin ABC"};
    
    public static void main(String[] args)
    {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;  
       
       
       while(true) {
    	   
    	   
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();

           // Geldeinwurf
           // -----------
           eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

           // Fahrscheinausgabe
           // -----------------
          fahrkarteAusgeben();

           // Rückgeldberechnung und -Ausgabe
           // -------------------------------
          rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                              "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir wünschen Ihnen eine gute Fahrt.\n\n");
       }
       
       
    
    }
    public static double fahrkartenbestellungErfassen() {
    	
    	System.out.println("\nWählen Sie Ihre Fahrkarte aus:\n");
    	for(int i = 0; i < bezeichnung.length; i++) {
    		System.out.println((i+1) + " " + bezeichnung[i] );}
    	
       	double zuZahlenderBetrag;
          zuZahlenderBetrag = karteWaehlen();
           
           zuZahlenderBetrag *= fahrkartenanzahlPrüfen();

           return zuZahlenderBetrag;
       }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    	{
    		System.out.printf("Noch zu zahlen: " + "%.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    		eingeworfeneMünze = tastatur.nextDouble();
    		eingezahlterGesamtbetrag += eingeworfeneMünze;
    	}
    	return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkarteAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f Euro\n", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
  
            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	  rückgabebetrag -= 1.00;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	  rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	  rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	  rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	  rückgabebetrag -= 0.05;
            }
        }
    }
    
    public static double fahrkartenanzahlPrüfen() {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.print("Anzahl der Tickets: ");
    	double kartenAnzahl = tastatur.nextDouble();
    	if(kartenAnzahl > 0 && kartenAnzahl <= 10) {
    		return kartenAnzahl;
    	}else {
    		System.out.println("Die Anzahl der Tickets muss zwischen 1 und  10 liegen. Anzahl wurde auf 1 gesetzt. ");
    		return 1;
    	}
    }
    
    public static double karteWaehlen() {
    	
    	Scanner tastatur = new Scanner(System.in);
    	System.out.print("\nIhre Wahl: ");
    	int kartenWahl = tastatur.nextInt();
    	double zuZahlenderBetrag;
    	if(kartenWahl <= preis.length && kartenWahl > 0) {
    	zuZahlenderBetrag = preis[kartenWahl -1];   	
    	return zuZahlenderBetrag;	}
    	else { 
    		return preis[0];
    	}
    }
}